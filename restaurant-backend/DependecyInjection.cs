﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using restaurant_backend.Core.Manager.Auth;
using restaurant_backend.Core.Manager.Employee;
using restaurant_backend.Core.Manager.OrderManager;
using restaurant_backend.Core.Manager.Product;
using restaurant_backend.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend
{
    public static class DependecyInjection
    {
        public static void Inject(IServiceCollection services, IConfiguration Configuration)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            services.AddScoped<IAuthManager, AuthManager>();
            services.AddScoped<IProductManager, ProductManager>();
            services.AddScoped<IEmployeeManager, EmployeeManager>();
            services.AddScoped<IOrderManager, OrderManager>();
        }

    }
}
