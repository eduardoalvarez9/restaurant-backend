﻿using restaurant_backend.Data.Models;
using restaurant_backend.Models.Order;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace restaurant_backend.Core.Manager.OrderManager
{
    public interface IOrderManager
    {
        Task CreateOrder(OrderRequestModel request);
        Task<IEnumerable<Order>> GetOrders();
        Task<Order> GetOrderDetails(int id);
    }
}
 