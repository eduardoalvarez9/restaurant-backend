﻿using Microsoft.EntityFrameworkCore;
using restaurant_backend.Data;
using restaurant_backend.Data.Models;
using restaurant_backend.Data.Repositories;
using restaurant_backend.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Core.Manager.OrderManager
{
    public class OrderManager : IOrderManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public OrderManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task CreateOrder(OrderRequestModel request)
        {
            try {
                var order = new Order
                {
                    CustomerName = request.CustomerName,
                    WaiterId = request.WaiterId,
                    TableId = request.TableId,
                    IsForDelivery = request.IsForDelivery,
                    Date = DateTime.Now,
                    Invoiced = false,
                    OrderDetails = request.OrderDetails.Select(x => new OrderDetail
                    {
                        ProductId = x.ProductId,
                        Quantity = x.Quantity,
                    }).ToList(),
                };


                await _repositoryWrapper.Order.CreateAsync(order);
                await _repositoryWrapper.SaveChangesAsync();

                
            }
            catch (Exception ex) 
                {
                throw ex;
            }
        }

        public async Task<IEnumerable<Order>> GetOrders()
        {
            return (await _repositoryWrapper.Order.GetAllAsync()).Where(s => s.Invoiced == false);
        }

        public async Task<Order> GetOrderDetails(int id)
        {
            var order = (await _repositoryWrapper.Order.GetEntity().Include(s => s.OrderDetails).FirstOrDefaultAsync(s => s.Id == id));
            foreach (var item in order.OrderDetails)
            {
                item.Product = _repositoryWrapper.Product.FindByCondition(s => s.Id == item.ProductId).FirstOrDefault();
            }
            return order;
        }
    }
}
