﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace restaurant_backend.Core.Manager.Product
{
    public interface IProductManager
    {
        Task<Data.Models.Product> CreateProductAsync(Data.Models.Product product);

        Task<Data.Models.Product> UpdateProductAsync(Data.Models.Product product);

        Task<IEnumerable<Data.Models.Product>> GetAllProductsAsync(string filter);

        Task<Data.Models.Product> GetProductByIdAsync(int id);

        Task DeleteProductAsync(Data.Models.Product product);
    }
}
