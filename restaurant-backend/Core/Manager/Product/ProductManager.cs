﻿using Microsoft.EntityFrameworkCore;
using restaurant_backend.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Core.Manager.Product
{
    public class ProductManager : IProductManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;
        public ProductManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<Data.Models.Product> CreateProductAsync(Data.Models.Product product)
        {
            try 
            {
                await _repositoryWrapper.Product.CreateAsync(product);
                await _repositoryWrapper.SaveChangesAsync();
                return product;
            }
            catch(Exception ex) {
                throw ex;
            }
        }

        public async Task<Data.Models.Product> UpdateProductAsync(Data.Models.Product product)
        {
            try
            {
                _repositoryWrapper.Product.Update(product);
                await _repositoryWrapper.SaveChangesAsync();
                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Data.Models.Product>> GetAllProductsAsync(string filter)
        {
            if (String.IsNullOrEmpty(filter))
            {
                return await _repositoryWrapper.Product.GetAllAsync();
            }
            else
            {
                return (await _repositoryWrapper.Product.GetAllAsync())
                    .Where(s => s.Name.ToLower().Contains(filter.ToLower())).ToList();
            }
        }

        public async Task<Data.Models.Product> GetProductByIdAsync(int id)
        {
            return await _repositoryWrapper.Product.FindByCondition(s => s.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task DeleteProductAsync(Data.Models.Product product)
        {
            try
            {
                _repositoryWrapper.Product.Delete(product);
                await _repositoryWrapper.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    }
}
