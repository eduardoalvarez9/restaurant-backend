﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace restaurant_backend.Core.Manager.Employee
{
    public interface IEmployeeManager
    {
        Task<Data.Models.Employee> GetEmployeeById(int id);
        Task<IEnumerable<Data.Models.Employee>> GetAllAsync();
        Task<IEnumerable<Data.Models.Employee>> GetPotentialWaitersAsync();
        Task<IEnumerable<Data.Models.Waiter>> GetWaitersAsync();
        Task AsignWaitersAsync(int[] ids);
        Task UnasignWaitersAsync(int[] ids);
        Task<IEnumerable<Data.Models.Employee>> GetPotentialCashiersAsync();
        Task<IEnumerable<Data.Models.Cashier>> GetCashiersAsync();
        Task AsignCashiersAsync(int[] ids);
        Task UnasignCashiersAsync(int[] ids);
        Task<Data.Models.Employee> CreateEmployeeAsync(Data.Models.Employee employee);
        Task<Data.Models.Employee> UpdateEmployeeAsync(Data.Models.Employee employee);
        Task DeleteEmployeeAsync(Data.Models.Employee employee);
    }
}
