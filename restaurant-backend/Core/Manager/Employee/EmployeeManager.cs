﻿using Microsoft.EntityFrameworkCore;
using restaurant_backend.Data.Models;
using restaurant_backend.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Core.Manager.Employee
{
    public class EmployeeManager : IEmployeeManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;
        public EmployeeManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task AsignCashiersAsync(int[] ids)
        {
            var newCashiers = ids.Select(s => new Cashier
            {
                EmployeeId = s,
            });
            await _repositoryWrapper.Cashier.CreateRangeAsync(newCashiers);
            await _repositoryWrapper.SaveChangesAsync();
        }

        public async Task AsignWaitersAsync(int[] ids)
        {
            var newWaiters = ids.Select(s => new Waiter
            {
                EmployeeId = s,
            });

            await _repositoryWrapper.Waiter.CreateRangeAsync(newWaiters);
            await _repositoryWrapper.SaveChangesAsync();
        }

        public async Task<Data.Models.Employee> CreateEmployeeAsync(Data.Models.Employee employee)
        {
            try
            {
                await _repositoryWrapper.Employee.CreateAsync(employee);
                await _repositoryWrapper.SaveChangesAsync();
                return employee;
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public async Task DeleteEmployeeAsync(Data.Models.Employee employee)
        {
            _repositoryWrapper.Employee.Delete(employee);
            await _repositoryWrapper.SaveChangesAsync();
        }

        public async Task<IEnumerable<Data.Models.Employee>> GetAllAsync()
        {
            return await _repositoryWrapper.Employee.GetAllAsync();
        }

        public async Task<IEnumerable<Cashier>> GetCashiersAsync()
        {
            return (await _repositoryWrapper.Cashier.GetEntity().ToListAsync())
                           .Join((await _repositoryWrapper.Employee.GetAllAsync()),
                           c => c.EmployeeId,
                           e => e.Id,
                           (c, e) => new Cashier
                           {
                               Id = c.Id,
                               EmployeeId = c.EmployeeId,
                               Employee = e
                           });
        }

        public async Task<Data.Models.Employee> GetEmployeeById(int id)
        {
            return await _repositoryWrapper.Employee.FindByCondition(s => s.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Data.Models.Employee>> GetPotentialCashiersAsync()
        {
            var cashiers = await _repositoryWrapper.Cashier.GetAllAsync();
            var employees = (await _repositoryWrapper.Employee.GetAllAsync())
                                .GroupJoin(cashiers,
                                      emp => emp.Id,
                                      ca => ca.EmployeeId,
                                      (emp, ca) => new { Employees = emp, Cashier = ca })
                                .SelectMany(s => s.Cashier.DefaultIfEmpty(),
                                    (s, c) => new
                                    {
                                        Employee = s.Employees,
                                        cashier = c
                                    }).Where(w => w.cashier == null).Select(s => s.Employee);
            return employees;
        }

        public async Task<IEnumerable<Data.Models.Employee>> GetPotentialWaitersAsync()
        {
            var waiters = await _repositoryWrapper.Waiter.GetAllAsync();
            var employees = (await _repositoryWrapper.Employee.GetAllAsync())
                                .GroupJoin(waiters,
                                      emp => emp.Id,
                                      wait => wait.EmployeeId,
                                      (emp, wait) => new { Employees = emp, Waiters = wait })
                                .SelectMany(s => s.Waiters.DefaultIfEmpty(),
                                    (s, w) => new
                                    {
                                        Employee = s.Employees,
                                        waiter = w  
                                    }).Where(w => w.waiter == null).Select(s => s.Employee);
            return employees;
        }

        public async Task<IEnumerable<Waiter>> GetWaitersAsync()
        {
            return (await _repositoryWrapper.Waiter.GetEntity().ToListAsync())
                            .Join((await _repositoryWrapper.Employee.GetAllAsync()),
                            w => w.EmployeeId,
                            e => e.Id,
                            (w, e) => new Waiter
                            {
                                Id = w.Id,
                                EmployeeId = w.EmployeeId,
                                Employee = e
                            });
                            
        }

        public async Task UnasignCashiersAsync(int[] ids)
        {
            foreach (var id in ids)
            {
                var cashier = await _repositoryWrapper.Cashier.FindByCondition(s => s.Id.Equals(id)).FirstOrDefaultAsync();
                _repositoryWrapper.Cashier.Delete(cashier);
            }
            await _repositoryWrapper.SaveChangesAsync();
        }

        public async Task UnasignWaitersAsync(int[] ids)
        {
            foreach (var id in ids)
            {
                var waiter = await _repositoryWrapper.Waiter.FindByCondition(s => s.Id.Equals(id)).FirstOrDefaultAsync();
                _repositoryWrapper.Waiter.Delete(waiter);
            }
            await _repositoryWrapper.SaveChangesAsync();
        }

        public async Task<Data.Models.Employee> UpdateEmployeeAsync(Data.Models.Employee employee)
        {
            try
            {
                _repositoryWrapper.Employee.Update(employee);
                await _repositoryWrapper.SaveChangesAsync();
                return employee;
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }
    }
}
