﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using restaurant_backend.Data.Models;

namespace restaurant_backend.Core.Manager.Auth
{
    public interface IAuthManager
    {
        Task<User> FindByUserName(string userName);
        Task<User> Register(User user, string password);
        Task<User> Login(string userName, string password);
        Task<bool> UserExist(string userName);

        string DoLogin(User user);

    }
}
