﻿using Microsoft.AspNetCore.Mvc;
using restaurant_backend.Core.Manager.Auth;
using restaurant_backend.Data.Models;
using restaurant_backend.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IAuthManager _authManager;

        public AuthController(IAuthManager authManager)
        {
            _authManager = authManager;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUser user)
        {
            // Validar el usuario
            user.UserName = user.UserName.ToLower();

            if (await _authManager.UserExist(user.UserName))
            {
                return BadRequest("El nombre de usuario ya existe.");
            }
            if (user.Password != user.ConfirmPassword)
            {
                return BadRequest("Las contraseñas no coiciden.");
            }

            var userToCreate = new User
            {
                UserName = user.UserName
            };

            var createdUser = await _authManager.Register(userToCreate, user.Password);
            return Created("Creado", createdUser);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginUser user)
        {
            var userFromRepo = await _authManager.Login(user.UserName.ToLower(), user.Password);

            if (userFromRepo == null)
            {
                return Unauthorized("Usuario o contraseña incorrectos.");
            }

            return Ok(new
            {
                token = _authManager.DoLogin(userFromRepo)
            });
        }

    }
}
