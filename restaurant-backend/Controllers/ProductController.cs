﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using restaurant_backend.Core.Manager.Product;
using restaurant_backend.Data.Models;
using restaurant_backend.Models.Products;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductManager _productManager;

        public ProductController(IProductManager productManager)
        {
            _productManager = productManager;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateProductModel productRequest)
        {
            if (productRequest == null)
            {
                return BadRequest("El producto esta vacío");
            }
            var product = new Product
            {
                Name = productRequest.Name,
                Price = productRequest.Price,
                Image = productRequest.Image,
                Description = productRequest.Description,
            };

            var newProduct = await _productManager.CreateProductAsync(product);
            return Ok(newProduct);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id ,[FromBody] CreateProductModel productRequest)
        {
            if (productRequest == null)
            {
                return BadRequest("El producto esta vacío");
            }
            var product = new Product
            {
                Id = id,
                Name = productRequest.Name,
                Price = productRequest.Price,
                Image = productRequest.Image,
                Description = productRequest.Description,
            };

            var newProduct = await _productManager.UpdateProductAsync(product);
            return Ok(newProduct);
        }

        [HttpGet("AllProducts/{filter}")]
        public async Task<IEnumerable<Product>> GetAllAsync(string filter)
        {
            var products = await _productManager.GetAllProductsAsync(filter);

            return products;
        }

        [HttpGet("AllProducts")]
        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            var products = await _productManager.GetAllProductsAsync(null);

            return products;
        }

        [HttpGet("GetById/{id}")]
        public async Task<Product> GetByIdAsync(int id)
        {
            var product = await _productManager.GetProductByIdAsync(id);

            return product;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var product = await _productManager.GetProductByIdAsync(id);
            await _productManager.DeleteProductAsync(product);
            return Ok();
        }
    }
}
