﻿using Microsoft.AspNetCore.Mvc;
using restaurant_backend.Core.Manager.Employee;
using restaurant_backend.Data.Models;
using restaurant_backend.Models.Employee;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace restaurant_backend.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeManager _employeeManager;
        public EmployeeController(IEmployeeManager employeeManager)
        {
            _employeeManager = employeeManager;
        }

        [HttpGet("AllEmployees")]
        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            return await _employeeManager.GetAllAsync();
        }

        [HttpGet("GetPotentialWaiters")]
        public async Task<IEnumerable<Employee>> GetPotentialWaitersAsync()
        {
            return await _employeeManager.GetPotentialWaitersAsync();
        }

        [HttpGet("GetWaiters")]
        public async Task<IEnumerable<Waiter>> GetWaitersAsync()
        {
            return (await _employeeManager.GetWaitersAsync());
        }

        [HttpPost("AsignWaiters")]
        public async Task<IActionResult> AsignWaitersAsync(int[] ids)
        {
            try
            {
                await _employeeManager.AsignWaitersAsync(ids);
                return Ok();
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        [HttpPost("UnasignWaiters")]
        public async Task<IActionResult> UnasignWaitersAsync(int[] ids)
        {
            try
            {
                await _employeeManager.UnasignWaitersAsync(ids);
                return Ok();
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        [HttpGet("GetPotentialCashiers")]
        public async Task<IEnumerable<Employee>> GetPotentialCashiersAsync()
        {
            return await _employeeManager.GetPotentialCashiersAsync();
        }

        [HttpGet("GetCashiers")]
        public async Task<IEnumerable<Cashier>> GetCashiersAsync()
        {
            return (await _employeeManager.GetCashiersAsync());
        }

        [HttpPost("AsignCashiers")]
        public async Task<IActionResult> AsignCashierAsync(int[] ids)
        {
            try
            {
                await _employeeManager.AsignCashiersAsync(ids);
                return Ok();
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        [HttpPost("UnasignCashiers")]
        public async Task<IActionResult> UnasignCashiersAsync(int[] ids)
        {
            try
            {
                await _employeeManager.UnasignCashiersAsync(ids);
                return Ok();
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        [HttpGet("GetById/{id}")]
        public async Task<Employee> GetByIdAsync(int id)
        {
            return await _employeeManager.GetEmployeeById(id);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateEmployeeModel employeeRequest)
        {
            if(employeeRequest == null)
            {
                return BadRequest("El producto esta vacío");
            }

            var employee = new Employee
            {
                Names = employeeRequest.Names,
                LastNames = employeeRequest.LastNames,
                IdNumber = employeeRequest.IdNumber
            };

            var newEmployee = await _employeeManager.CreateEmployeeAsync(employee);
            return Ok(newEmployee);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id,[FromBody] Employee employee)
        {
            if (employee == null)
            {
                return BadRequest("El producto esta vacío");
            }
            employee.Id = id;

            var updatedEmployee = await _employeeManager.UpdateEmployeeAsync(employee);
            return Ok(updatedEmployee);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var employee = await _employeeManager.GetEmployeeById(id);
            await _employeeManager.DeleteEmployeeAsync(employee);
            return Ok();
        }
    }
}
