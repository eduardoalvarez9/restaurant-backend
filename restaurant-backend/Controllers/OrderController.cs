﻿using Microsoft.AspNetCore.Mvc;
using restaurant_backend.Core.Manager.OrderManager;
using restaurant_backend.Data.Models;
using restaurant_backend.Models.Order;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace restaurant_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly IOrderManager _orderManager;
        public OrderController(IOrderManager orderManager)
        {
            _orderManager = orderManager;
        }

        [HttpPost("Create")]
        public async Task<IActionResult> CreateOrder([FromBody] OrderRequestModel orderRequest)
        {
            await _orderManager.CreateOrder(orderRequest);
            return Ok();
        }

        [HttpGet]
        public async Task<IEnumerable<Order>> GetOrders()
        {
            return await _orderManager.GetOrders();
        }

        [HttpGet("details/{id}")]
        public async Task<Order> GetOrderDetails(int id)
        {
            return await _orderManager.GetOrderDetails(id);
        }
    }
}
