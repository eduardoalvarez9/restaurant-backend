﻿namespace restaurant_backend.Models.Employee
{
    public class CreateEmployeeModel
    {
        public int IdNumber { get; set; }

        public string Names { get; set; }

        public string LastNames { get; set; }
    }
}
