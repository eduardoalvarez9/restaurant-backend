﻿using System.ComponentModel.DataAnnotations;

namespace restaurant_backend.Models.Products
{
    public class CreateProductModel
    {
        [Required(ErrorMessage = "El nombre de producto es requerido")]
        public string Name { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }
    }
}
