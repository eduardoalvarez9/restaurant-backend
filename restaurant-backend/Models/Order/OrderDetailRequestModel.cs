﻿namespace restaurant_backend.Models.Order
{
    public class OrderDetailRequestModel
    {
        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public string Observations { get; set; }
    }
}
