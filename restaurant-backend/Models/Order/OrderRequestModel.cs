﻿using System.Collections.Generic;

namespace restaurant_backend.Models.Order
{
    public class OrderRequestModel
    {
        public string CustomerName { get; set; }
        public int WaiterId { get; set; }
        public int TableId { get; set; }
        public bool IsForDelivery { get; set; }
        public IEnumerable<OrderDetailRequestModel> OrderDetails { get; set; }

    }
}
