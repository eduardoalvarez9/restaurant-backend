﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Models.Auth
{
    public class LoginUser
    {
        [Required(ErrorMessage = "El nombre de usuario es requerido.")]
        public string UserName { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "La longitud de contraseña es entre 4 a 16 caracteres.")]
        public string Password { get; set; }

    }
}
