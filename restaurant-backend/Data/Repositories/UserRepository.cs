﻿using restaurant_backend.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Data.Repositories
{
    public class UserRepository : RepositoryBase<User>
    {
        public UserRepository(RestauranDataContext context) : base(context)
        {
        }
    }

}
