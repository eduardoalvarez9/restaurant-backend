﻿using restaurant_backend.Data.Models;

namespace restaurant_backend.Data.Repositories
{
    public class ProductRepository : RepositoryBase<Product>
    {
        public ProductRepository(RestauranDataContext context) : base(context)
        {
        }
    }
}
