﻿using restaurant_backend.Data.Models;

namespace restaurant_backend.Data.Repositories
{
    public class CashierRepository : RepositoryBase<Cashier>
    {
        public CashierRepository(RestauranDataContext context) : base(context)
        {
        }
    }
}
