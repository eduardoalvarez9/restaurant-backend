﻿using Microsoft.EntityFrameworkCore;
using restaurant_backend.Data.Models;

namespace restaurant_backend.Data.Repositories
{
    public class WaiterRepository : RepositoryBase<Waiter>
    {
        public WaiterRepository(RestauranDataContext context) : base(context)
        {
        }
    }
}
