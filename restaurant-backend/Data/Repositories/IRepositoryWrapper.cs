﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Data.Repositories
{
    public interface IRepositoryWrapper
    {
        UserRepository User { get; }

        ProductRepository Product { get; }

        EmployeeRepository Employee { get; }

        WaiterRepository Waiter { get; }

        CashierRepository Cashier { get; }

        OrderRepository Order { get; }

        Task SaveChangesAsync();

    }
}
