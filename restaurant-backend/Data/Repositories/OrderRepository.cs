﻿using restaurant_backend.Data.Models;

namespace restaurant_backend.Data.Repositories
{
    public class OrderRepository : RepositoryBase<Order>
    {
        public OrderRepository(RestauranDataContext context) : base(context)
        {
        }
    }
}
