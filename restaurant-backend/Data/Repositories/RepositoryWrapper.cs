﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Data.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RestauranDataContext _context;
        private UserRepository _user;
        private ProductRepository _product;
        private EmployeeRepository _employee;
        private WaiterRepository _waiter;
        private CashierRepository _cashier;
        private OrderRepository _order;

        public RepositoryWrapper(RestauranDataContext context)
        {
            _context = context;
        }

        public UserRepository User
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserRepository(_context);
                }
                return _user;
            }
        }

        public ProductRepository Product
        {
            get
            {
                if(_product == null)
                {
                    _product = new ProductRepository(_context);
                }
                return _product;
            }
        }

        public EmployeeRepository Employee
        {
            get
            {
                if (_employee == null)
                {
                    _employee = new EmployeeRepository(_context);
                }
                return _employee;
            }
        }

        public WaiterRepository Waiter
        {
            get
            {
                if(_waiter == null)
                {
                    _waiter = new WaiterRepository(_context);
                }
                return _waiter;
            }
        }

        public CashierRepository Cashier
        {
            get
            {
                if (_cashier == null)
                {
                    _cashier = new CashierRepository(_context);
                }
                return _cashier;
            }
        }

        public OrderRepository Order
        {
            get
            {
                if (_order == null)
                {
                    _order = new OrderRepository(_context);
                }
                return _order;
            }
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

    }
}
