﻿using restaurant_backend.Data.Models;

namespace restaurant_backend.Data.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>
    {
        public EmployeeRepository(RestauranDataContext context) : base(context)
        {
        }
    }
}
