﻿using Microsoft.EntityFrameworkCore;
using restaurant_backend.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Data
{
    public class RestauranDataContext : DbContext
    {
        public RestauranDataContext( DbContextOptions options) 
            : base(options) { }

        public DbSet<User> Users { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Table> Tables { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderDetail> OrderDetails { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Cashier> Cashier { get; set; }

        public DbSet<Waiter> Waiters { get; set; }

        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<InvoiceDetail> InvoicesDetail { get; set; }

        public DbSet<Payment> Payments { get; set; }
    }
}
