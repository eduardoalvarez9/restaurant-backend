﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace restaurant_backend.Data.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Order")]
        public int OrderId { get; set; }

        [ForeignKey("Cashier")]
        public int CashierId { get; set; }

        public DateTime Date { get; set; }

        public bool Invoiced { get; set; }

        public virtual Order Order { get; set; }

        public virtual Cashier Cashier { get; set; }

        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }

        public virtual Payment Payment { get; set; }
    }
}
