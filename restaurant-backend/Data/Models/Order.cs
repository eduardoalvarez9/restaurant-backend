﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace restaurant_backend.Data.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Table")]
        public int TableId { get; set; }

        [ForeignKey("Waiter")]
        public int WaiterId { get; set; }

        public string CustomerName { get; set; }

        public bool IsForDelivery { get; set; }

        public bool Invoiced { get; set; }

        public double Total { get; set; }

        public DateTime Date { get; set; }

        public virtual Table Table { get; set; }

        public virtual Waiter Waiter { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
