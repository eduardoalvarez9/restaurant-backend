﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace restaurant_backend.Data.Models
{
    public class Payment
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Invoice")]
        public int InvoiceId { get; set; }

        public double Amount { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}
