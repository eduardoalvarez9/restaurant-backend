﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace restaurant_backend.Data.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        public int IdNumber { get; set; }

        public string Names { get; set; }

        public string LastNames { get; set; }

        public virtual ICollection<Waiter> Waiters { get; set;}

        public virtual ICollection<Cashier> Cashiers { get; set; }
    }
}
