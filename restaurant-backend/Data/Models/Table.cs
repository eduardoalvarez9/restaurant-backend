﻿using System.ComponentModel.DataAnnotations;

namespace restaurant_backend.Data.Models
{
    public class Table
    {
        [Key]
        public int Id { get; set; }

        public bool IsInUse { get; set; }

        public int ActualOrderId { get; set; }
    }
}
