﻿using restaurant_backend.Core.Manager.Auth;
using restaurant_backend.Data.Models;
using restaurant_backend.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restaurant_backend.Data
{
    public class Seed
    {
        public static async void SeedDatabase(RestauranDataContext context, IRepositoryWrapper repository, IAuthManager authRepository)
        {
            await CreateUser(context, repository, authRepository);
            await CreateTables(context);
            await CreateEmployees(context);
            await CreateProductsAsync(context);
            await CreateOrders(context);
            await repository.SaveChangesAsync();

        }

        private static async Task CreateUser(RestauranDataContext context, IRepositoryWrapper repository, IAuthManager authRepository)
        {
            if (!context.Users.Any())
            {
                var user = new User
                {
                    UserName = "admin",
                };

                await authRepository.Register(user, "Qwerty1234@");
            }
        }

        private static async Task CreateEmployees(RestauranDataContext context)
        {
            if (!context.Employees.Any()) 
            {
                var Employees = new List<Employee>
                {
                    new Employee
                    {
                        IdNumber = 12,
                        Names = "Jorge",
                        LastNames = "Alvarez",
                    },
                    new Employee
                    {
                        IdNumber = 13,
                        Names = "Arnulfo",
                        LastNames = "Guzman",
                    },
                    new Employee
                    {
                        IdNumber = 14,
                        Names = "Jose",
                        LastNames = "Perez",
                    },
                    new Employee
                    {
                        IdNumber = 15,
                        Names = "Karla",
                        LastNames = "Zelaya",
                    },
                    new Employee
                    {
                        IdNumber = 16,
                        Names = "Pedro",
                        LastNames = "Valdez",
                    },
                };
                await context.Employees.AddRangeAsync(Employees);

               
            }
            if (context.Employees.Any())
            {
                if (!context.Waiters.Any())
                {
                   var waiters = new List<Waiter> {
                       new Waiter
                       {
                           EmployeeId = 1,
                       },
                       new Waiter
                       {
                           EmployeeId = 2,
                       },
                       new Waiter
                       {
                           EmployeeId = 3,
                       }
                   };
                   await context.Waiters.AddRangeAsync(waiters);
                }

                if (!context.Cashier.Any())
                {
                    var cashiers = new List<Cashier>
                {
                    new Cashier
                    {
                        EmployeeId = 3
                    },
                    new Cashier
                    {
                        EmployeeId= 4,
                    }
                };
                    await context.Cashier.AddRangeAsync(cashiers);
                }
            }
        }

        private static async Task CreateTables(RestauranDataContext context)
        {
            if (!context.Tables.Any())
            {
                var tables = new List<Table>
                {
                    new Table
                    {
                        IsInUse = false
                    },
                    new Table
                    {
                        IsInUse = false
                    },
                    new Table
                    {
                        IsInUse = false
                    },
                    new Table
                    {
                        IsInUse = false
                    },
                    new Table
                    {
                        IsInUse = false
                    }
                };

                await context.Tables.AddRangeAsync(tables);
            }
        }

        private static async Task CreateProductsAsync(RestauranDataContext context)
        {
            if (!context.Products.Any())
            {
                var Products = new List<Product>
                {
                    new Product{
                     Name = "Carne Asada",
                     Description = "Producto 1",
                     Price = 150d
                    },
                    new Product{
                     Name = "Chuleta Asada",
                     Description = "Producto 1",
                     Price = 150d
                    },
                    new Product{
                     Name = "Pollo Asado",
                     Description = "Producto 1",
                     Price = 140d
                    },
                    new Product{
                     Name = "Carne Mixta Ch",
                     Description = "Producto 1",
                     Price = 190d
                    },
                    new Product{
                     Name = "Chuleta Mixta",
                     Description = "Producto 1",
                     Price = 200d
                    },
                    new Product{
                     Name = "Mar y tierra",
                     Description = "Producto 1",
                     Price = 250d
                    }
                };

                await context.Products.AddRangeAsync(Products);
            }
        }

        private static async Task CreateOrders(RestauranDataContext context)
        {
            if (!context.Orders.Any())
            {
                var orders = new List<Order>();
                Random r = new Random();
                for (int i = 0; i < 1000; i++)
                {
                    var details = new List<OrderDetail>();
                    for(int j = 0; j <= r.Next(1,5); j++)
                    {
                        var productId = r.Next(1, 6);
                        details.Add(new OrderDetail { ProductId = productId, Quantity = r.Next(1, 10), Product = context.Products.Find(productId)});
                    }
                    orders.Add(new Order
                    {
                        CustomerName = "Cliente Final",
                        WaiterId = r.Next(1,3),
                        TableId = r.Next(1,5),
                        IsForDelivery = false,
                        Date = DateTime.Now,
                        Invoiced = true,
                        Total = details.Sum(s => (s.Product.Price * s.Quantity)),
                        OrderDetails = details
                    });
                }
                for (int i = 0; i < 2000; i++)
                {
                    var details = new List<OrderDetail>();
                    for (int j = 0; j <= r.Next(1, 5); j++)
                    {
                        var productId = r.Next(1, 6);
                        details.Add(new OrderDetail { ProductId = productId, Quantity = r.Next(1, 10), Product = context.Products.Find(productId) });
                    }
                    orders.Add(new Order
                    {
                        CustomerName = "Cliente Final",
                        WaiterId = r.Next(1, 3),
                        TableId = r.Next(1, 5),
                        IsForDelivery = true,
                        Date = DateTime.Now,
                        Invoiced = true,
                        Total = details.Sum(s => (s.Product.Price * s.Quantity)),
                        OrderDetails = details
                    });
                }
                await context.Orders.AddRangeAsync(orders);    
            }
        }
    }
}
